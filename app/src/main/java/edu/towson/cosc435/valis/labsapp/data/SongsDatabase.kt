package edu.towson.cosc435.valis.labsapp.data

import androidx.lifecycle.LiveData
import androidx.room.*
import edu.towson.cosc435.valis.labsapp.model.Song

@Dao
interface SongsDao {
    @Query("SELECT id, name, artist, track, is_awesome, icon_url FROM song")
    suspend fun getSongs(): List<Song>

    @Insert
    suspend fun addSong(song: Song)

    @Delete
    suspend fun deleteSong(song: Song)

    @Update
    suspend fun updateSong(song: Song)
}

@Database(entities = [Song::class], version = 2, exportSchema = false)
abstract class SongsDatabase : RoomDatabase() {
    abstract fun songDao(): SongsDao
}
