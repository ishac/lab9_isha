package edu.towson.cosc435.valis.labsapp.data.impl

import edu.towson.cosc435.valis.labsapp.data.ISongsRepository
import edu.towson.cosc435.valis.labsapp.model.Song
import edu.towson.cosc435.valis.labsapp.network.ISongsFetcher

class SongsApiWithDatabase (
    private val songsDatabase: SongsDatabaseRepository,
    private val songsFetcher: ISongsFetcher
): ISongsRepository {
    override suspend fun getSongs(): List<Song> {
        return songsFetcher.fetchSongs()
    }

    override suspend fun deleteSong(song: Song) {
        TODO("Not yet implemented")
    }

    override suspend fun addSong(song: Song) {
        TODO("Not yet implemented")
    }

    override suspend fun toggleAwesome(song: Song) {
        TODO("Not yet implemented")
    }
}