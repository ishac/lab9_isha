package edu.towson.cosc435.valis.labsapp.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Song(
    @PrimaryKey
    val id: String,
    val name: String,
    val artist: String,
    val track: Int,
    @ColumnInfo(name = "is_awesome")
    @SerializedName("is_awesome")
    val isAwesome: Boolean,
    @ColumnInfo(name = "icon_url")
    @SerializedName("icon_url")
    val iconUrl: String?
)